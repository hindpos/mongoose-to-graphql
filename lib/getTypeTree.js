/* eslint-disable no-use-before-define */
import { forOwn, intersection } from 'lodash';

const setDescendant = (tree, key, value) => {
  let parentTree = tree;

  // Make sure there is an object for each of the ancestors
  // Ex. 'location.address.street1'' -> { location: { address: {} } }
  const splitPath = key.split('.');
  for (let i = 0; i < splitPath.length - 1; i += 1) {
    const ancestor = splitPath[i];
    parentTree[ancestor] = parentTree[ancestor] || {};
    parentTree = parentTree[ancestor];
  }

  const property = splitPath[splitPath.length - 1];
  parentTree[property] = value;
};

const instanceToType = (instance, validators, options) => {
  switch (instance) {
    case 'Boolean':
      return 'Boolean';
    case 'ObjectID':
      if (options) {
        if (options.ref) {
          return 'JSON2';
        }
      }
      return 'String';
    case 'String':
      return 'String';
    case 'Date':
      return 'Date';
    case 'Number':
      if (validators) {
        const funcList = validators.map(validator => validator.validator.name);
        const intFuncList = ['isSafeInteger', 'isInteger'];
        if (intersection(funcList, intFuncList).length > 0) {
          return 'Int';
        }
      }
      return 'Float';
    case 'Mixed':
      return 'JSON';
    default:
      throw new Error(`${instance} not implemented yet in instanceToType`);
  }
};

const arrayToTree = (path) => {
  if (path.caster && path.caster.instance) {
    return [instanceToType(path.caster.instance)];
  } else if (path.casterConstructor) {
    return [getTypeTree(path.casterConstructor.schema.paths)];
  }

  throw new Error(`${path} is not a supported path`);
};

const getTypeTree = (schemaPaths) => {
  const typeTree = {};

  forOwn(schemaPaths, (path, key) => {
    if (key === '__v') {
      return;
    }
    if (key === '_id' && path.instance === 'Mixed') {
      return;
    }
    let value;
    if (path.instance === 'Array') {
      value = arrayToTree(path);
    } else if (path.instance === 'Embedded') {
      value = getTypeTree(path.caster.schema.paths);
    } else {
      value = instanceToType(path.instance, path.validators, path.options);
    }
    setDescendant(typeTree, key, value);
  });

  return typeTree;
};

export default getTypeTree;
