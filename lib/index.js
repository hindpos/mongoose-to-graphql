/* eslint-disable import/prefer-default-export */
import { find, forOwn } from 'lodash';

import getType from './getType';
import getUpdateType from './getUpdateType';
import getCreateType from './getCreateType';
import getTypeObjects from './getTypeObjects';
import getTypeTree from './getTypeTree';
import resolvers from './resolvers';
import typeDefs from './typeDefs';

export const modelToType = (model, options = {}) => {
  const schema = model.schema;
  const typeTree = getTypeTree(schema.paths);

  const typeObjects = getTypeObjects(options.name || model.modelName, typeTree);
  if (options.extend) {
    forOwn(options.extend, (extension, type) => {
      const typeObject = find(typeObjects, t => t.type === type);
      Object.assign(typeObject.properties, extension);
    });
  }

  const typeStrings = typeObjects.map(getType);
  return typeStrings.join('\n');
};

export const modelToCreateType = (model, options = {}) => {
  const schema = model.schema;
  const typeTree = getTypeTree(schema.paths);

  const typeObjects = getTypeObjects(options.name || model.modelName, typeTree);
  if (options.extend) {
    forOwn(options.extend, (extension, type) => {
      const typeObject = find(typeObjects, t => t.type === type);
      Object.assign(typeObject.properties, extension);
    });
  }

  const typeStrings = typeObjects.map(getCreateType);
  return typeStrings.join('\n');
};

export const modelToUpdateType = (model, options = {}) => {
  const schema = model.schema;
  const typeTree = getTypeTree(schema.paths);

  const typeObjects = getTypeObjects(options.name || model.modelName, typeTree);
  if (options.extend) {
    forOwn(options.extend, (extension, type) => {
      const typeObject = find(typeObjects, t => t.type === type);
      Object.assign(typeObject.properties, extension);
    });
  }

  const typeStrings = typeObjects.map(getUpdateType);
  return typeStrings.join('\n');
};

export {
  resolvers,
  typeDefs,
};
