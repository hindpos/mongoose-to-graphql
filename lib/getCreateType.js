const convertToCreateType = (value) => {
  if (!value) {
    return value;
  }
  if (value === 'JSON2') {
    return 'String';
  }
  const dataTypes = ['Int', 'Float', 'String', 'Boolean', 'Date', 'JSON'];

  const isType = type => dataTypes
      .map(T => T === type)
      .reduce((a, c) => (a || c));
  const isAlphabetic = type => /^[a-zA-Z]+$/.test(type);
  const isArrayType = (type) => {
    if (type.length < 2) {
      return false;
    }
    return (type[0] === '[' && type[type.length - 1] === ']');
  };
  const arrayTypeSlice = type => type.substring(1, type.length - 1);

  if (isAlphabetic(value)) {
    if (!isType(value)) {
      return `${value}Create`;
    }
  } else if (isArrayType(value)) {
    const insideArray = arrayTypeSlice(value);
    if (isAlphabetic(insideArray)) {
      if (!isType(insideArray)) {
        return `[${insideArray}Create]`;
      }
    }
  }

  return value;
};

const getCreateType = (typeObject) => {
  let typeString = `input ${typeObject.type}Create {\n`;

  Object.keys(typeObject.properties)
        .sort()
        .forEach((key) => {
          const value = convertToCreateType(typeObject.properties[key]);
          typeString += `  ${key}: ${value}\n`;
        });

  typeString += '}';

  return typeString;
};

export default getCreateType;
