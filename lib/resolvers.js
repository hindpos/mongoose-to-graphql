import { GraphQLDateTime } from 'graphql-iso-date';
import GraphQLJSON from 'graphql-type-json';

const resolvers = {
  Date: GraphQLDateTime,
  JSON: GraphQLJSON,
};

export default resolvers;
