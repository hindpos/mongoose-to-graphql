/* eslint-disable import/prefer-default-export */

const getType = (typeObject) => {
  let typeString = `type ${typeObject.type} {\n`;

  Object.keys(typeObject.properties)
    .sort()
    .forEach((key) => {
      let value = typeObject.properties[key];
      if (value === 'JSON2') {
        value = 'JSON';
      }
      typeString += `  ${key}: ${value}\n`;
    });

  typeString += '}';

  return typeString;
};

export default getType;
