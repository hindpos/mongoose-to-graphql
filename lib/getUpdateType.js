const convertToUpdateType = (key, value) => {
  if (!value) {
    return value;
  }
  if (key === '_id') {
    return value;
  }
  if (value === 'JSON2') {
    return 'String';
  }
  const dataTypes = ['Int', 'Float', 'String', 'Boolean', 'Date', 'JSON'];
  const rangeDataTypes = ['Int', 'Float', 'Date', 'String'];

  const isType = type => dataTypes
    .map(T => T === type)
    .reduce((a, c) => (a || c));
  const isRangeType = type => rangeDataTypes
    .map(T => T === type)
    .reduce((a, c) => a || c);
  const isAlphabetic = type => /^[a-zA-Z]+$/.test(type);
  const isArrayType = (type) => {
    if (type.length < 2) {
      return false;
    }
    return (type[0] === '[' && type[type.length - 1] === ']');
  };
  const arrayTypeSlice = type => type.substring(1, type.length - 1);

  if (isRangeType(value)) {
    return `${value}Range`;
  }
  if (isAlphabetic(value)) {
    if (!isType(value)) {
      return `${value}Update`;
    }
  } else if (isArrayType(value)) {
    const insideArray = arrayTypeSlice(value);
    if (isAlphabetic(insideArray)) {
      if (!isType(insideArray)) {
        return `[${insideArray}Update]`;
      }
    }
  }

  return value;
};

const getUpdateType = (typeObject) => {
  let typeString = `input ${typeObject.type}Update {\n`;

  Object.keys(typeObject.properties)
    .sort()
    .forEach((key) => {
      const value = convertToUpdateType(key, typeObject.properties[key]);
      typeString += `  ${key}: ${value}\n`;
    });

  typeString += '}';

  return typeString;
};

export default getUpdateType;
