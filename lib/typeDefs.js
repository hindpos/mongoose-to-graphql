const IntRangeType = `input IntRange{
  gte: Int
  lte: Int
  eq: Int
}`;

const FloatRangeType = `input FloatRange{
  gte: Float
  lte: Float
  eq: Float
}`;

const DateRangeType = `input DateRange{
  gte: Date
  lte: Date
  eq: Date
}`;

const StringRangeType = `input StringRange{
  regex: String
  options: String
  nin: [String]
  eq: String
}`;

const customScalars = [
  'scalar Date',
  'scalar JSON',
];

export default {
  input: `${IntRangeType}
  ${FloatRangeType}
  ${DateRangeType}
  ${StringRangeType}`,
  scalar: customScalars,
};
