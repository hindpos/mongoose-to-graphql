import test from 'ava';
import getUpdateType from '../lib/getUpdateType';

test('converts a type object to a type string', (t) => {
  const typeObject = {
    type: 'OrderNote',
    properties: {
      author: 'String',
      message: 'String',
    },
  };

  const typeString = `input OrderNoteUpdate {
  author: StringRange
  message: StringRange
}`;

  t.is(getUpdateType(typeObject), typeString);
});
