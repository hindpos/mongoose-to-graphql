/* eslint-disable import/no-extraneous-dependencies */
import test from 'ava';
import { modelToType, modelToUpdateType, modelToCreateType } from '../lib/index';
import {
  // UserModel,
  // UserType,
  SongModel,
  SongType,
  BookCreateTypes,
  BookModel,
  BookTypes,
  BookTypesExtended,
  NotebookTypes,
  OrderModel,
  OrderTypes,
  StoreModel,
  StoreType,
  StockModel,
  StockType,
  StockListModel,
  StockListUpdateType } from './models';

// test('flat UserModel with required and _id:false type to a type string', (t) => {
//   const schema = modelToType(UserModel);
//   t.is(schema, UserType, `Expected\n${schema}\nto equal\n${UserType}`);
// });

test('flat SongModel with reference converts to a type string', (t) => {
  const schema = modelToType(SongModel);
  t.is(schema, SongType);
});

test('flat StoreModel converts to a type string', (t) => {
  const schema = modelToType(StoreModel);
  t.is(schema, StoreType);
});

test('flat StockModel contains a Integer type', (t) => {
  const schema = modelToType(StockModel);
  t.is(schema, StockType, `Expected\n${schema}\nto equal\n${StockType}`);
});

test('flat StockModel contains a Integer type', (t) => {
  const schema = modelToUpdateType(StockListModel);
  t.is(schema, StockListUpdateType, `Expected\n${schema}\nto equal\n${StockListUpdateType}`);
});

test('nested OrderModel converts to a type string', (t) => {
  const schema = modelToType(OrderModel);
  t.is(schema, OrderTypes, `Expected\n${schema}\nto equal\n${OrderTypes}`);
});

test('embedded BookModel converts to a type string', (t) => {
  const schema = modelToType(BookModel);
  t.is(schema, BookTypes, `Expected\n${schema}\nto equal\n${BookTypes}`);
});

test('can extend generated types', (t) => {
  const schema = modelToType(BookModel, {
    extend: {
      Book: {
        publishers: '[Publisher]',
      },
      BookCategory: {
        genre: 'Genre',
      },
    },
  });

  t.is(schema, BookTypesExtended, `Expected\n${schema}\nto equal\n${BookTypesExtended}`);
});

test('can overwrite generated type name', (t) => {
  const schema = modelToType(BookModel, {
    name: 'Notebook',
  });

  t.is(schema, NotebookTypes, `Expected\n${schema}\nto equal\n${NotebookTypes}`);
});

test('embedded BookModel converts to a input type string', (t) => {
  const schema = modelToCreateType(BookModel);
  t.is(schema, BookCreateTypes, `Expected\n${schema}\nto equal\n${BookCreateTypes}`);
});
