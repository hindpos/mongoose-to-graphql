/* eslint-disable import/no-extraneous-dependencies */
import mongoose, { Schema } from 'mongoose';

export const SongModel = mongoose.model('Song', new Schema({
  singer: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Singer',
  },
}));

export const SongType = `type Song {
  _id: String
  singer: JSON
}`;

export const StoreModel = mongoose.model('Store', new Schema({
  joinDate: Date,
  name: String,
  no: Number,
  placeId: String,
}));

export const StoreType = `type Store {
  _id: String
  joinDate: Date
  name: String
  no: Float
  placeId: String
}`;

const StockSchema = new Schema({
  product: String,
  quantity: {
    type: Number,
    validate: {
      validator: Number.isSafeInteger,
    },
  },
  price: Number,
});

export const StockModel = mongoose.model('Stock', StockSchema);

export const StockListModel = mongoose.model('StockList', new Schema({
  stock: [StockSchema],
  total: {
    type: Number,
    validate: {
      validator: Number.isSafeInteger,
    },
  },
}));

export const StockType = `type Stock {
  _id: String
  price: Float
  product: String
  quantity: Int
}`;

export const StockListUpdateType = `input StockListStockUpdate {
  _id: String
  price: FloatRange
  product: StringRange
  quantity: IntRange
}
input StockListUpdate {
  _id: String
  stock: [StockListStockUpdate]
  total: IntRange
}`;

export const UserModel = mongoose.model('User', new Schema({
  _id: false,
  username: {
    type: String,
    required: true,
  },
  name: String,
  books: {
    type: [String],
    required: true,
  },
}));

export const UserType = `type User  {
  books: [String]!
  name: String
  username: String!
}`;

export const OrderModel = mongoose.model('Order', new Schema({
  name: String,
  location: {
    deliveryInstructions: String,
    placeId: String,
  },
  statusHistory: [{
    note: String,
    status: String,
  }],
  tags: [String],
}));

export const OrderTypes = `type OrderStatusHistory {
  _id: String
  note: String
  status: String
}
type OrderLocation {
  deliveryInstructions: String
  placeId: String
}
type Order {
  _id: String
  location: OrderLocation
  name: String
  statusHistory: [OrderStatusHistory]
  tags: [String]
}`;

const CategorySchema = new Schema({
  type: String,
});

export const BookModel = mongoose.model('Book', new Schema({
  category: CategorySchema,
  name: String,
}));

export const BookTypes = `type BookCategory {
  _id: String
  type: String
}
type Book {
  _id: String
  category: BookCategory
  name: String
}`;

export const BookTypesExtended = `type BookCategory {
  _id: String
  genre: Genre
  type: String
}
type Book {
  _id: String
  category: BookCategory
  name: String
  publishers: [Publisher]
}`;

export const NotebookTypes = `type NotebookCategory {
  _id: String
  type: String
}
type Notebook {
  _id: String
  category: NotebookCategory
  name: String
}`;

export const BookCreateTypes = `input BookCategoryCreate {
  _id: String
  type: String
}
input BookCreate {
  _id: String
  category: BookCategoryCreate
  name: String
}`;
